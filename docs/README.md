# Extras
---

Performance,Utilities,Monitoring, etc tools for Devuan..
This tools will be used in Single Board Computers and maybe others device images..

#### Table of contents
* [Introduction](#introduction)
* [Characteristics](#characteristics)
* [Maintainer](#maintainer)

### Introduction:
----
This repository will hold tools to support Devuan images..

Since motd, to Temperature readings, etc

### Characteristics:
----
* motd for devuan Beowulf          **/etc/update-motd.d/{10-uname,20-devuan}**
* UPS monitoring for Olimex Olime2 **/etc/update-motd.d/30-monitor**
* WIFI WPA_SUPPLICANT config       **/etc/wpa_supplicant/wpa_supplicant-wlan0.conf**

### Maintainer
----
maintainer:	tuxd3v

